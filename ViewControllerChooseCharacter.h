//
//  ViewControllerChooseCharacter.h
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 11/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerChooseCharacter : UIViewController

{
    IBOutlet UIImage *personagem1;
    IBOutlet UIImage *personagem2;


    IBOutlet UIImageView *imageArea;

}

@property(nonatomic, retain)UIImage *personagem1;
@property(nonatomic, retain)UIImage *personagem2;
@property(nonatomic, retain)UIImageView *imageArea;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;

-(IBAction)Button1;
-(IBAction)Button2;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *itBeginsButton;
@property (weak, nonatomic) IBOutlet UIButton *timeToGoButton;

@end