//
//  HistoryChangeViewController.h
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 13/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryChangeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imagemCena;
@property (weak, nonatomic) IBOutlet UITextView *textoHistoria;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;

- (IBAction)rightPressed:(id)sender;
- (IBAction)leftPressed:(id)sender;

@end
