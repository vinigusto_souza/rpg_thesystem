//
//  Cena1.h
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 13/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cena1 : UIViewController{
    NSArray *history;
    int currentSelected;
}

@property (weak, nonatomic) IBOutlet UIImageView *imagemCena01;
@property (weak, nonatomic) IBOutlet UITextView *textoCena01;
@property (weak, nonatomic) IBOutlet UIButton *LeftButton;
@property (weak, nonatomic) IBOutlet UIButton *RightButton;

    -(IBAction)RightPressed:(id)sender;
    -(IBAction)LeftPressed:(id)sender;

@end
