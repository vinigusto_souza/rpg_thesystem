//
//  BotaoCena01ViewController.m
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 13/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import "BotaoCena01ViewController.h"

@interface BotaoCena01ViewController ()

@end

@implementation BotaoCena01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)voltar{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
