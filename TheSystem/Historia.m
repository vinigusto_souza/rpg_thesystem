//
//  Historia.m
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 13/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import "Historia.h"

@interface Historia ()

@implementation

-(instancetype)initWithNome:(NSString*)history{
    if(self = [super init]){
        
        self.history = history;
        
    }
    return self;
}

-(instancetype)initWithNome:(NSString *)history ImageFileName:(NSString*) imgFilename{
    if(self = [self initWithHistory:history]){
        _imgFileName = imgFilename;
    }
    return self;
}

@end
