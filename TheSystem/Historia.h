//
//  Historia.h
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 13/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Historia : NSObject



@property NSString* history;
@property NSString *imgFileName;

-(instancetype)initWithNome:(NSString*)history;
-(instancetype)initWithNome:(NSString*)history ImageFileName:(NSString*) imgFilename;

@end
