//
//  VoltarViewController.h
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 16/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoltarViewController : UIViewController

-(IBAction)voltar;

@end
