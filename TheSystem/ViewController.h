//
//  ViewController.h
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 11/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *logar;
@property (weak, nonatomic) IBOutlet UITextField *entryUsername;
@property (weak, nonatomic) IBOutlet UILabel *titleTheSystem;
@property (weak, nonatomic) IBOutlet UIButton *comecar;
@property (weak, nonatomic) IBOutlet UIImageView *imagemTelaInicial;

    -(IBAction)editingChanged;

@end

