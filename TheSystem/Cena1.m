//
//  Cena1.m
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 13/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import "Cena1.h"

@interface Cena1 ()

@end

@implementation Cena1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self Popular];
    
    currentSelected = 0;
    
    [self LoadSelectedHistory];
    
    
    [self ChangeToHistoryWithIndex:currentSelected];
    
}

-(void)ChangeToHistoryWithIndex:(int)index{
    if(index < 0 || index >= minhaHistoria.count){
        NSLog(@"Erro a historia com indice %d não existe",index);
        return;
        
    }
    
    Historia *current = [minhaHistoria objectAtIndex:index];
    
    _NomeLabel.text = [NSString stringWithFormat:@"Nome: %@",current.nome] ;
    _FotoImage.image = [UIImage imageNamed:current.imgFileName];
    _ClasseLabel.text = [NSString stringWithFormat:@"Classe: %@",[current.tipo uppercaseString]] ;
    _DescricaoText.text = [current GetDescricao];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)Popular{
    minhaHistoria = [[NSArray alloc]initWithObjects:
                  [[history alloc]initWithNome:@"Binder" Tipo:@"Chefe" DescricaoFileName:@"binder.txt" ImageFileName:@"binder.png" Armadura:@"Manto Mágico" Arma:nil],
                  [[history alloc]initWithNome:@"Micheli" Tipo:@"Ranger" DescricaoFileName:@"micheli.txt" ImageFileName:@"micheli.png" Armadura:@"Colete Mágico" Arma:@"Arco de Fogo"],
                  nil];
    
    
    
}

- (IBAction)LeftPressed:(id)sender {
    if(--currentSelected <= 0){
        currentSelected = 0;
        _LeftButton.hidden = YES;
    }
    _RightButton.hidden = NO;
    [self SaveSelectedHistory];
    [self ChangeToHistoryWithIndex:currentSelected];
}

- (IBAction)RightPressed:(id)sender {
    if(++currentSelected > minhaHistoria.count-2){
        currentSelected = (int)minhaHistoria.count-1;
        _RightButton.hidden = YES;
    }
    _LeftButton.hidden = NO;
    [self SaveSelectedHistory];
    [self ChangeToHistoryWithIndex:currentSelected];
}

-(void)LoadSelectedHistory{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
    
    
    
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"currentSelected.txt"];
    
    NSError *error;
    
    NSString *currentSelectedString = [[NSString alloc] initWithContentsOfFile:documentsDirectory encoding:NSUTF8StringEncoding error:&error];
    
    if (currentSelectedString == nil){
        // Handle error here
        NSLog(@"Erro:%@",[error description]);
    }else{
        currentSelected = [currentSelectedString intValue];
        
    }
}

-(void)SaveSelectedHistory{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents directory
    
    NSLog(@"%@",documentsDirectory);
    
    
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"currentSelected.txt"];
    
    NSError *error;
    NSString *myString = [NSString stringWithFormat:@"%d",currentSelected];
    BOOL succeed = [myString writeToFile:documentsDirectory
                              atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (!succeed){
        // Handle error here
        NSLog(@"Erro:%@",[error description]);
    }
}
@end
