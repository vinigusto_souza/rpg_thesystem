//
//  Personagem.m
//  RPG
//
//  Created by Mark Joselli on 3/4/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "Personagem.h"

@implementation Personagem

-(instancetype)initWithNome:(NSString*)nome{
    if(self = [super init]){
        
        self.nome = nome;
        
    }
    return self;
}

-(instancetype)initWithNome:(NSString *)nome Tipo:(NSString *)tipo DescricaoFileName:(NSString*)descricaoFileName ImageFileName:(NSString*) imgFilename{
    if(self = [self initWithNome:nome]){
        self.tipo = tipo;
        _descricaoFileName = descricaoFileName;
        _imgFileName = imgFilename;
    }
    return self;
}

-(instancetype)initWithNome:(NSString *)nome Tipo:(NSString *)tipo DescricaoFileName:(NSString*)descricaoFileName ImageFileName:(NSString*) imgFilename Armadura:(NSString *)armadura Arma:(NSString *)arma{
    if(self = [self initWithNome:nome]){
        self.tipo = tipo;
        _descricaoFileName = descricaoFileName;
        _imgFileName = imgFilename;
        
        if(armadura != nil)
            [self AddEquipment:@"Armadura" withPoderes:armadura];
        if(arma != nil)
            [self AddEquipment:@"Arma" withPoderes:arma];
    }
    return self;
}

-(void)AddEquipment:(NSString *)tipo withPoderes:(NSString *)descricao{
    if(self.equipamento == nil){
        self.equipamento = [[NSMutableDictionary alloc]init];
    }
    
    [_equipamento setObject:descricao forKey:tipo];
}

-(NSString *)GetDescricao{
    NSArray *descriptionParts = [_descricaoFileName componentsSeparatedByString:@"."];
    
    NSURL *URL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:descriptionParts[0] ofType:descriptionParts[1]]];
    
    NSError *error;
    
    NSString *descricao = [[NSString alloc] initWithContentsOfURL:URL encoding:NSUTF8StringEncoding error:&error];
    
    if(descricao == nil){
        NSLog(@"Error:%@",[error description]);
        
        return @"";
    }
    
    descricao = [descricao stringByAppendingString:@"\n\nEquipamentos:\n"];
    
    if(_equipamento != nil){
        NSArray *keys = [_equipamento allKeys];
        NSArray *sortedKeys = [keys sortedArrayUsingSelector:@selector(compare:)];
        for (NSString *key in sortedKeys) {
            descricao = [descricao stringByAppendingString:[NSString stringWithFormat:@"%@ - %@ \n",key,[_equipamento objectForKey:key]]];
            
        }
        
        
    }
    
    return descricao;

}

@end
