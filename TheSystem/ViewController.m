//
//  ViewController.m
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 11/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _comecar.enabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ChangeTextClick:(id)sender {
    
    NSString *welcome = @"WELCOME, ";
    NSString *entryUsername = self.entryUsername.text.capitalizedString;
    NSString *result = [welcome stringByAppendingString:entryUsername];
    self.titleTheSystem.text = result;
    [self.entryUsername resignFirstResponder];
}

-(IBAction)editingChanged{
    if([_entryUsername.text length] != 0){
        _comecar.enabled = YES;
    }
  
}

@end
