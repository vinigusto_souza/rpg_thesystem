//
//  ViewControllerChooseCharacter.m
//  TheSystem
//
//  Created by Vinicius Augusto de Souza on 11/03/15.
//  Copyright (c) 2015 Vinícius Augusto de Souza. All rights reserved.
//

#import "ViewControllerChooseCharacter.h"


@interface ViewControllerChooseCharacter ()

@end

@implementation ViewControllerChooseCharacter

@synthesize personagem1, personagem2, imageArea;

-(IBAction)Button1{
    imageArea.image = personagem1;
}

-(IBAction)Button2{
    imageArea.image = personagem2;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    personagem1 = [UIImage imageNamed:@"personagem1portrait.jpg"];
    personagem2 = [UIImage imageNamed:@"personagem2portrait2.png"];
    
    _itBeginsButton.hidden = true;
    _timeToGoButton.hidden = true;
    

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)personagem1Selecionado:(id)sender {
    
    _nameLabel.text = @"ESTA AVENTURA AINDA NÃO ESTÁ DISPONÍVEL";
    //time to go
    _timeToGoButton.hidden = TRUE;
    _itBeginsButton.hidden = true;
    
}

-(IBAction)personagem2Selecionado:(id)sender {
    
    _nameLabel.text = @"THE ENFORCER";
    //and so it begins
    _itBeginsButton.hidden = false;
    _timeToGoButton.hidden = true;

}

@end
